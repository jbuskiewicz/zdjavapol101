// let a = 0;
// let a = '';
// let a = {};

// number, string, boolean, null, undefined
// object, array, function

// console.log(document.getElementById("contactForm"));

// var, let, const
// var - przestrzale, uzywane w tzw. VanillaJs
// let, const - ES6 w gore

// transpilacja - zamiana nowej wersji na starą wersje - sluzy do tego np. Babel
let formContainer = document.getElementById("contactForm"); // zwracany poj. wezel z dokumentu HTML

// hoisting
// 1 przebieg - zbiera informacje na temat zadeklarowanych zmiennych, funkcji itd

// console.log(addTwoNumbers);
// console.log(number);

function addTwoNumbers(a, b) {
  return a + b;
}

// inicjalizacja, deklaracja - dwie rozne
var number = 10; // zasieg globalny - zmienna var
// console.log(number);

// console.log(numb2); // blad - proba odniesienia sie do zmiennej przed inicjalizacja - let i const obejmuje temporary dead zone
let numb2 = 20; // zasieg blokowy
const URL = "http://google.com"; // zasieg blokowy

formContainer.classList.add("formularz");
// formContainer.classList.remove("formularz");

formContainer.addEventListener("submit", (event) => {
  event.preventDefault(); // wylaczamy domyslne zachowanie eventu
  //   console.log(event);
  //
  const [firstName, lastName, email, message] = event.target;
  const formState = {
    validationMessages: {
      lastName: "Podaj nazwisko",
      email: "Podaj prawidlowy adres mailowy",
      message: "Wiadomosc powinna miec min. 120 znakow",
    },
  };

  if (lastName.value.length === 0) {
    console.log(formState.validationMessages.lastName);
    const messageElement = document.createElement("p"); // <p></p>
    messageElement.innerText = formState.validationMessages.lastName; // <p>Podaj nazwisko</p>
    messageElement.classList.add("validationMessage");
    lastName.classList.add("error");
    lastName.parentElement.appendChild(messageElement);
  } else {
    lastName.classList.remove("error");
    document.querySelector(".validationMessage").innerHTML = "";
  }
});

// API - Application programming interface
